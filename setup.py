import platform
from aezef.version import VERSION

if platform.system() != 'Windows':
    # Unfortunately, this stalled on Windows when I tested it...
    import ez_setup
    ez_setup.use_setuptools(version='32.0.0')

from setuptools import setup

requirements = [
    'configparser',     # backport of python 3.5 version
    'future',
    'numpy',
    'pandas',
    'six',
]

long_description = '''
aezef
=======

* TBD


How do I get set up?
----------------------

* ``pip install aezef``

  Alternatively, clone the repository or download the tarball and run this command
  on the included setup.py file:

    ``python setup.py install``


Contribution guidelines
------------------------

* TBD

Who do I talk to?
------------------

* Rich Plevin (rich@plevin.com)
'''

setup(
    name='aezef',
    version=VERSION,
    description='Python 2.7 library and scripts computing carbon intensity of land-use change with GTAP',
    platforms=['Windows', 'Mac OS X', 'Linux'],

    packages=['aezef'],
    entry_points={'console_scripts': ['ci = aezef.main:main']},
    install_requires=requirements,
    include_package_data = True,

    url='https://bitbucket.org/plevin/aezef',
    download_url='https://plevin@bitbucket.org/plevin/aezef.git',
    license='MIT License',
    author='Richard Plevin',
    author_email='rich@plevin.com',

    classifiers=[
          'Development Status :: 4 - Beta',
          'License :: OSI Approved :: MIT License',
          'Intended Audience :: Science/Research',
          ],

    zip_safe=True,
)
