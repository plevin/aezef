# AEZ-EF : Agro-ecological Zone Emission Factor Model #

This repo contains a Python version of the AEZ-EF (agroecological zone emission factor) model extended from the version produced for CARB to handle bioenergy crops. The extensions were made for Wally Tyner to support work on aviation biofuels. See the file `AEZ-EF_Model_Notes_v4.pdf` for a description of changes to accommodate bioenergy crops.

## What is it? ##

The `aezef` package computes "carbon intensity" (CI) from the results of a static GTAP-BIO model run by estimating the CO2 emissions over a given number of years (generally 30) to each possible land-use transition assumed from changes in land-cover projected by GTAP in response to a given biofuel shock. The total emissions in CO2 is divided by the energy content of the shock over the given number of years to produce a result in units of g CO2 / MJ of fuel shock.

Note that the model considers only the CO2 changes in soil and biomass, plus estimates of emissions of other greenhouse gases (GHGs) from burning biomass to clear land. Emissions from changes in agricultural energy use, fertilizer use, and rebound effects in fuel and other markets are _not_ included.

## How does it work? ##

First you run GTAP, then, with configuration file variables directed to the GTAP output files, you run the python script, potentially with some command-line arguments to control other behavior.

The Python code calls gempack command-line utilities to convert HAR files to text, and then parses their contents (see gempack.py). The configuration file allows you to set parameters like the name of the Experiment, the Feedstock shocked, and so on.

## How do I get set up? ##

Use "git" to clone the repository, then (assuming you've installed python) run:

	python setup.py

to install the `aezef` package, along with a script called `ci` (carbon intensity) that is the command-line entry-point to the program.


### Who do I talk to? ###

* Rich Plevin (rich@plevin.com)
