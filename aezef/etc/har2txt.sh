#!/usr/bin/env bash

# Do nothing but report success. This is useful if gempack programs
# like har2txt are not available (e.g., on Mac OS).
exit 0
