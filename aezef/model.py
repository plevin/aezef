'''
@author: Richard Plevin

Copyright (c) 2012-2017. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
from __future__ import print_function
from logging import getLogger
import numpy as np
import os
import pandas as pd
from subprocess import call

from .chart import plotSummaries
from .config import setParam, setSection, getParam, getParamAsBoolean, getParamAsFloat, getParamAsInt
from .gempack import readHarFile
from .emissions import PastureToCrop, ForestToCrop
from .transition import Transition, LAND_USES, CELLULOSIC

from .utils import (
    gtapDataFrame, getFileParams, CO2PerC, GasolineMJperGal, getUnits, setUnits, mkdirs,
    REG, SWITCHGRASS, MISCANTHUS, POPLAR, FOR, PAS, CP, ANN, SUG, PLM, OPP, MSC, SWG, POP,
    LAND_HEADERS, FORESTRY, CROPLAND, PASTURE, ALLCROPS, CROPPAST, SUGARCROP, OILPALM,
    SpecialCrops
)

# Status codes for invoked programs
SUCCESS = 0
FAILURE = 1

_logger = getLogger(__name__)

def writeDF(f, name, df, fmt="%.0f"):
    totalSeries = df.sum()
    grandTotal  = totalSeries.sum()

    units = getUnits(df)
    f.write("\n%s%s:" % (name, " (%s)" % units if units else ''))
    if grandTotal == 0.0:
        f.write(" All zeros\n")
        return

    f.write('\n')

    # Reduce the headings to first 7 chars for cleaner output
    cols = df.columns
    df.columns = map(lambda col: col[:7], cols)
    f.write(df.to_csv(None, sep='\t', float_format=fmt))
    # f.write(df.to_csv(None, sep=',', float_format=fmt))
    df.columns = cols
    f.write('TOTAL')
    for idx in totalSeries.index:
        f.write("\t%.0f" % totalSeries[idx])
    f.write("\t%.0f" % grandTotal)
    f.write('\n')


class Model(object):
    '''
    The AEZ-EF model
    '''
    instance = None

    def __init__(self, args):
        Transition.decache()    # forget prior results to RandomizeTransitions

        Model.instance = self

        self.args = args

        self.exp       = getParam('Experiment')
        self.feedstock = getParam('Feedstock')
        self.finalFuel = 'Gasoline' # shocks are now entered in GGE
        self.shockGGE  = getParamAsFloat('ShockGGE')
        self.cropNames = getParam('CropNames').split(',')

        self.outputDir = os.path.join(getParam('GtapDir'), self.exp)
        mkdirs(self.outputDir)

        self.emissionFactors = None
        self.landChange      = None

        # fuelDensity  = fuelDensityMjPerGal(args.finalFuel)
        self.shockMJ = self.shockGGE * GasolineMJperGal

        self.params = getFileParams()
        self.readLandCover()    # read GTAP land-use change output

        self.transDict = self.createTransDict()

        self.forest  = ForestToCrop(self)
        self.pasture = PastureToCrop(self)

        self.computeEmissionFactors()
        self.computeLandChanges()
        self.computeEmissions()

        self.ilucIntensity = self.computeCI()

        if not getParamAsBoolean('RandomizeTransitions') and args.dumpData:
            self.dumpData()

    def createTransDict(self):
        invalidTransitions = (
            [{lu, lu} for lu in LAND_USES] +
            [{old, new} for old in (PLM, SUG) for new in CELLULOSIC] +
            [{CP, FOR}]
        )

        Transition.initialize(self)
        transDict = {}

        def isValid(oldLU, newLU):
            return {oldLU, newLU} not in invalidTransitions

        def addTrans(old, new):
            obj = Transition.get(old, new)
            transDict[obj.key] = obj

        # Special cases for transition to oil palm on peat
        for old, new in [(FOR, OPP), (PAS, OPP)]:
            addTrans(old, new)

        for old in LAND_USES:
            for new in LAND_USES:
                if isValid(old, new):
                    addTrans(old, new)

        return transDict

    def computeEmissionFactors(self):
        '''
        Compute emission factors for all possible land transitions. In this version,
        sugarcane and oil palm (2 perennial crops) are treated separately from the
        annual crops, and palm is treated specially in the case of forest-to-palm
        conversion, which is treated as causing loss of peatland in regions defined
        as having peatland (currently only AEZs 4-6 in the Mala_Indo region.)
        '''
        forest  = self.forest
        pasture = self.pasture

        # Do these first because methods below refer to values calculated here
        forest.computeConversionToCropland()
        pasture.computeConversionToCropland()

        forest.computeConversionToPasture(pasture)
        pasture.computeConversionToForest(forest)

        self.emissionFactors = {obj.name: obj.emissionFactors() for obj in self.transDict.itervalues()}


    def makeTransition(self, source, sink, limit=None):
        '''
        Compute the changes associated with a changes in corresponding Region-AEZs in the
        given source and sink DataFrames. Assume the maximum change allowed by these values,
        which is the minimum of the source change and abs(sink change). Note that this method
        alters the caller's source and sink DataFrames.
        :param source: a land change DataFrame to be treated as a source for the transition
        :param sink: a land change DataFrame to be treated as the sink, with negatives indicating reduction
        :return: a DataFrame containing the hectares allocated from source to sink for each Region-AEZ
        '''
        relevant = (source < 0) & (sink > 0)
        net = source + sink         # presumed positive plus negatives
        net[(relevant == False)] = np.NaN  # leave out irrelevant Region-AEZs

        # if positive, source was greater than sink, else (-)sink was greater than source.
        sourceMask = (net <= 0)
        sinkMask   = (net >  0)

        # This effectively uses the min(sourceGreater, -sinkGreater).
        # Subtracting sink converts to positives so the values can be subtracted
        # from source and sink to remove these hectares from consideration
        changes = gtapDataFrame()
        changes[sourceMask] += sink[sourceMask]
        changes[sinkMask]   -= source[sinkMask]

        if limit is not None:
            # restrict the change further to the values in the DF passed
            d1 = changes - limit        # find where limit is less than computed transition
            d1[d1 < 0] = 0.0            # d1 holds max change in these cases
            d1[d1 > 0] = limit[d1 > 0]

            d2 = limit - changes        # find where limit is greater than computed transition
            d2[d2 < 0] = 0.0            # d2 holds max change in these cases
            d2[d2 > 0] = changes[d2 > 0]

            limitedChange = d1 + d2     # max change allowed for this transition
            limit[:] -= limitedChange   # modifies caller's DF to hold unallocated changes
            changes   = limitedChange   # use these restricted values below

        source[:] += changes            # modify caller's DataFrames
        sink[:]   -= changes

        return changes


    def computeLandChanges(self):
        '''
        Use heuristic method to allocate land transitions to the most likely candidates.
        Each transition subtracts the allocated land from the source and sink DataFrames
        so that subsequent transitions process only what remains.
        :return: None
        '''
        forestry    = self.landChange[FORESTRY].copy()        # make copies so we don't alter caller's DFs holding GTAP results
        pasture     = self.landChange[PASTURE].copy()
        cropland    = self.landChange[CROPLAND].copy()
        allcrops    = self.landChange[ALLCROPS]
        cropNames   = self.cropNames

        for crop in SpecialCrops:
            try:
                idx = cropNames.index(crop)
                matrix = allcrops[idx].copy()
            except ValueError:
                # If not present in current GTAP model, assign zeros
                matrix = gtapDataFrame()

            self.landChange[crop] = matrix

        croppast    = self.landChange[CROPPAST]
        oilpalm     = self.landChange[OILPALM]
        sugarcrop   = self.landChange[SUGARCROP]
        switchgrass = self.landChange[SWITCHGRASS]
        miscanthus  = self.landChange[MISCANTHUS]
        poplar      = self.landChange[POPLAR]

        # treat sugar beet as ordinary annual crop
        sugarcane = sugarcrop * self.params.fractionSugarAsCane

        # Subtract all crops that are treated explicitly. Note that none of these
        # currently has a harvest frequency different than 1, so harvested area is
        # equal to land-cover area in each crop. Not so for all annual crops, though.
        # However, since we subtract these from cropland (the landcover, not the area
        # harvested (possibly multiple times per year) the value is still correct.
        perennials = oilpalm + sugarcane + croppast + switchgrass + miscanthus + poplar
        annuals = cropland - perennials

        peatAreas = gtapDataFrame()
        peatAreas.Mala_Indo['AEZ4':'AEZ6'] = True

        trans = self.transDict

        # Handle conversion of forest- and pasture-to-palm on peat in MALA_INDO
        newPalmOnPeatland = oilpalm[(oilpalm > 0) & (peatAreas == True)].fillna(0) * self.params.MalaIndoOilPalmOnPeatFactor

        # Split out transitions to oil palm on peat (OPP)
        trans[(FOR, OPP)].area = self.makeTransition(forestry, oilpalm, limit=newPalmOnPeatland)
        trans[(PAS, OPP)].area = self.makeTransition(pasture,  oilpalm, limit=newPalmOnPeatland)

        # Map land-use names to DataFrames of area changes
        lu = {ANN: annuals,     FOR: forestry,   PAS: pasture,
              CP : croppast,    SUG: sugarcane,  PLM: oilpalm,
              SWG: switchgrass, MSC: miscanthus, POP: poplar}

        # N.B. makeTransition alters the DataFrames passed as arguments
        sequences = [
            # First, transitions to/from cropland-pasture since these are relatively easy.
            (ANN, CP),
            (SUG, CP),
            (SWG, CP),
            (MSC, CP),
            (POP, CP),
            (PLM, CP),

            (CP, ANN),
            (CP, SUG),
            (CP, SWG),
            (CP, MSC),
            (CP, POP),
            (CP, PLM),

            # Allow transitions among grasses and poplar
            (SWG, MSC),
            (MSC, SWG),
            (SWG, POP),
            (MSC, POP),
            (POP, SWG),
            (POP, MSC),

            # Next, transitions between annual, perennials, and grasses.
            # We assume no transitions between palm & sugar and cellulosic crops.
            (ANN, SUG),
            (ANN, PLM),
            (ANN, SWG),
            (ANN, MSC),
            (ANN, POP),

            (SUG, ANN),
            (PLM, ANN),
            (SWG, ANN),
            (MSC, ANN),
            (POP, ANN),

            # Allow changes between perennials
            (SUG, PLM),
            (PLM, SUG),

            # Transitions between crops and pasture
            (SWG, PAS),
            (MSC, PAS),
            (POP, PAS),
            (PAS, SWG),
            (PAS, MSC),
            (PAS, POP),

            (ANN, PAS),
            (SUG, PAS),
            (PLM, PAS),
            (PAS, ANN),
            (PAS, SUG),
            (PAS, PLM),    # (PAS, OPP) handled above

            # Transitions between pasture and forestry
            (FOR, PAS),
            (PAS, FOR),

            # Transitions between forestry and tree crops
            (FOR, POP),
            (FOR, PLM),    # (FOR, OPP) handled above
            (POP, FOR),
            (PLM, FOR),

            # Transitions between forestry and the rest
            (FOR, ANN),
            (FOR, SUG),
            (FOR, SWG),
            (FOR, MSC),
            (ANN, FOR),
            (SUG, FOR),
            (SWG, FOR),
            (MSC, FOR),
        ]

        if getParamAsBoolean('RandomizeTransitions'):
            import random
            random.shuffle(sequences)
            # sequences = firstGroup + randomGroup + restOfGroups

        for old, new in sequences:
            trans[(old, new)].area = self.makeTransition(lu[old], lu[new])

        # Reset cropland to anything that remains unallocated in these categories
        self.remainingCropland = annuals + sugarcane + oilpalm + switchgrass + miscanthus + poplar

        self.transitionOrder = [(FOR, OPP), (PAS, OPP)] + sequences + [(CP, PAS), (PAS, CP)]
        assert set(self.transitionOrder) == set(self.transDict.keys()), 'Error assigning to self.transitionOrder'


    def computeEmissions(self):
        '''
        Compute emissions by multiplying each emission factor by the area for
        the corresponding LUC transition.
        '''
        Transition.computeEmissions()

        transDict = self.transDict

        if getParam('SaveMatrices') and not getParamAsBoolean('RandomizeTransitions'):
            outputFile = os.path.join(self.outputDir, 'matrices.txt')
            transitions = [Transition.get(*pair) for pair in self.transitionOrder]
            with open(outputFile, 'w') as f:

                f.write("Transitions:\n")
                for obj in transitions:
                    setUnits(obj.area, 'ha')
                    writeDF(f, obj.name, obj.area)

                f.write("\n\nEmission Factors:\n")
                for obj in transitions:
                    writeDF(f, obj.name, obj.ef)

                f.write("\n\nEmissions:\n")
                for obj in transitions:
                    writeDF(f, obj.name, obj.emissions)

        self.emissions = [obj.emissions for obj in transDict.itervalues()]
        totalWithoutCrops = sum(map(lambda df: df.sum().sum(), self.emissions))  # sum across the list of DataFrame totals

        self.totalEmissionsWithoutCrops = totalWithoutCrops
        _logger.debug("LUC emissions before adding crop C: %.2E Mg CO2", self.totalEmissionsWithoutCrops)

        # Add in crop-region-AEZ-specific, average biomass for all areas in which
        # cropland increased. We use the post-sim yield given by GTAP to represent
        # the newly converted land. Convert to CO2 equivalents. Negative is because
        # we count emissions as positive and sequestration as negative in CO2 flows.
        postConvCropC   = self.avgPostConvCropC = self.landChange['TLBC']
        postConvCropCO2 = -1 * postConvCropC * CO2PerC
        _logger.info("Post-conversion crop biomass, C=%.0f [for AEZ-EF spreadsheet]", postConvCropC)
        _logger.debug("Post-conversion crop CO2e=%.0f Mg CO2", postConvCropCO2)

        self.totalEmissions = totalWithoutCrops + postConvCropCO2
        _logger.debug("Total emissions: %.2E Mg CO2e", self.totalEmissions)

    def writePlotData(self, outDir):
        '''
        Generate DataFrames by original LU type to all options,
        summed over AEZs
        '''
        transDict = self.transDict

        mkdirs(outDir)

        def zeros():
            return pd.Series(data=0.0, index=REG)

        for old in LAND_USES:
            area = []
            ef = []
            ghg = []

            for new in LAND_USES:
                pair = (old, new)
                try:
                    obj = transDict[pair]
                except KeyError:
                    obj = None

                s = obj.area.sum() if obj else zeros()
                s.name = new
                area.append(s)

                s = obj.emissions.sum() if obj else zeros()
                s.name = new
                ghg.append(s)

            oldLU = old.lower()
            d = dict(area=area, ghg=ghg)
            for key, rows in d.items():
                df = pd.DataFrame(data=rows)
                filename = os.path.join(outDir, "%s-%s.csv" % (key, oldLU))
                df.to_csv(filename)


    def readLandCover(self):
        # read in GTAP results in as matrices of land use changes indexed by coefficient short-name
        landChangeHarFile = getParam('LandChangeHarFile')

        # readHarFile returns a dict of 3D numpy arrays
        self.landChange = readHarFile(landChangeHarFile, LAND_HEADERS,
                                      overwrite=True, deleteTxt=False)

        # Convert the 2D matrices to DataFrames
        for key, value in self.landChange.iteritems():
            shape = value.shape
            if shape == (1,1,1):
                self.landChange[key] = value[0,0,0]     # grab single values from 3D array
            elif shape == (1,18,19):
                self.landChange[key] = gtapDataFrame(data=value[0])
            else:
                blocks = shape[0]
                self.landChange[key] = blockList = []
                for block in range(blocks):
                    blockList.append(gtapDataFrame(data=value[block]))

    def computeCI(self):
        """
        Calculate ILUC carbon intensity and print result. Must be called after "run" method
        """
        analyticHorizon = getParamAsInt('AnalyticHorizonYears')
        totalFuel_MJ = self.shockMJ * analyticHorizon

        _logger.debug("ExpName: %s, fuel: %s, shock: %.2e GGE, total fuel: %.1e MJ",
                      self.exp, self.finalFuel, self.shockGGE, totalFuel_MJ)

        totalGHGs = self.totalEmissions

        # ILUC is one-time, so use total fuel increment (30 y)
        ilucIntensity = totalGHGs * 1e6 / totalFuel_MJ

        _logger.info('Analytical horizon %d y, foregone sequestration: %d y',
                     analyticHorizon, getParamAsInt('ForegoneSequestYears'))
        print("ILUC intensity (%s): %.2f g CO2e/MJ" % (self.exp, ilucIntensity))
        return ilucIntensity

    def saveToExcel(self, writer, boldFormat, labelFormat):
        '''
        Save the data extracted from landcover.har in the form required for use with
        the Excel version of the AEZ-EF model.
        '''
        from openpyxl.utils import coordinate_to_tuple

        sheetName = self.exp
        wb = writer.book

        ws = wb.add_worksheet(name=sheetName)
        writer.sheets[sheetName] = ws # otherwise pandas level doesn't see it

        def saveDF(df, matrixName, addr):
            row, col = coordinate_to_tuple(addr)
            row -= 1    # to_excel uses zero-based indexing
            col -= 1
            df.to_excel(writer, index=True, sheet_name=sheetName,
                        startrow=row, startcol=col, float_format="%.4f")
            ws.write_string(row, col, matrixName, labelFormat)

        # Write the DataFrames
        saveDF(self.landChange[FORESTRY],    'cFORESTRY',    'A6')
        saveDF(self.landChange[PASTURE],     'cPASTURE',     'A27')
        saveDF(self.landChange[CROPLAND],    'cCROPLAND',    'A48')
        saveDF(self.landChange[CROPPAST],    'cCROPPAST',    'A69')
        saveDF(self.landChange[SUGARCROP],   'cSUGAR',       'A90')
        saveDF(self.landChange[OILPALM],     'cOILPALM',     'A111')
        saveDF(self.landChange[SWITCHGRASS], 'cSWITCHGRASS', 'A132')
        saveDF(self.landChange[MISCANTHUS],  'cMISCANTHUS',  'A153')
        saveDF(self.landChange[POPLAR],      'cPOPLAR',      'A174')

        # Create the labels
        labels = {'A1':'Name',
                  'A2':'Feedstock',
                  'A3':'Fuel',
                  'A4':'Increment',
                  'D3':'Non-CO2 emissions',
                  'D4':'Total biomass chg'}
        for addr, label in labels.iteritems():
            ws.write(addr, label, boldFormat)

        description = "%s %s, %g GGE" % (self.feedstock, self.finalFuel, self.shockGGE)

        ws.write('B1', description)
        ws.write('B2', self.feedstock)
        ws.write('B3', self.finalFuel)
        ws.write('B4', self.shockGGE)
        ws.write('C4', "(GGE)")
        ws.write('F3', 0)                       # non-CO2 is not currently used
        ws.write('F4', self.avgPostConvCropC)

    def dumpData(self):
        # Dump the dataframes to an excel workbook
        outputFile = os.path.join(self.outputDir, 'datalog.xlsx')
        _logger.info('Writing %s', outputFile)

        # hack the header_style to remove boxes and centering
        pd.formats.format.header_style = {"font": {"bold": True},
                                          "alignment": {"horizontal": "right"}}

        with pd.ExcelWriter(outputFile, engine='xlsxwriter') as writer:
            wb = writer.book

            boldFormat = wb.add_format({'bold': True})

            def writeDFs(obj, name):
                ws = wb.add_worksheet(name=name)
                writer.sheets[name] = ws # otherwise pandas level doesn't see it
                row = 0
                col = 0

                aDict = obj if isinstance(obj, dict) else obj.__dict__
                for key in sorted(aDict.keys()):
                    value = aDict[key]
                    if type(value) == pd.DataFrame:
                        # Copy add a totals row to a copy
                        totals = value.sum()
                        totals.name = 'TOTALS'
                        df = value.append(totals)

                        rows, cols = df.shape

                        df.to_excel(writer, index=True, sheet_name=name,
                                    startrow=row, startcol=col,
                                    float_format="%.4f")
                        ws.write_string(row, col, key, boldFormat)
                        row += rows + 2

            writeDFs(self, 'Model')
            writeDFs(self.params.dict, 'Parameters')
            writeDFs(self.forest,  'Forest')
            writeDFs(self.pasture, 'Pasture')
            # writeDFs(self.emissions, 'Emissions')

            efs   = {}
            ghgs  = {}
            trans = {}
            for obj in self.transDict.values():
                name = obj.name
                trans[name] = obj.area
                ghgs[name]  = obj.emissions
                efs[name]   = obj.ef

            writeDFs(efs,   'Emission factors')
            writeDFs(ghgs,  'Emissions')
            writeDFs(trans, 'Transitions')

            if getParamAsBoolean('SaveCropChanges'):
                # Save all other crops and their total as a cross check.
                landChanges = self.landChange['ALLC']
                crops = {name : landChanges[idx] for idx, name in enumerate(self.cropNames)}

                otherCrops = gtapDataFrame()
                for df in crops.values():
                    otherCrops += df
                crops['Total'] = otherCrops

                writeDFs(crops, 'Other crop changes')


def saveToExcel(resultsDict):
    '''
    Save the results of each experiment to a separate worksheet in an XLSX workbook.
    '''
    # hack the header_style to remove boxes and centering
    pd.formats.format.header_style = {"font": {"bold": True},
                                      "alignment": {"horizontal": "right"}}

    filename = getParam('ResultsWorkbook')

    with pd.ExcelWriter(filename, engine='xlsxwriter') as writer:
        _logger.info("Saving results to %s", filename)
        workbook = writer.book
        labelFormat = workbook.add_format({'bold': True,
                                           'font_color': 'green'})
        boldFormat = workbook.add_format({'bold': True})

        for exp, model in resultsDict.iteritems():
            model.saveToExcel(writer, boldFormat, labelFormat)


lucCmfTemplate = '''! input files
file GTAPSETS = "{gtapDir}/sets.har" ;
file GTAPDATA = "{gtapDir}/basedata.har" ;
file CROPSPEC = "{cropSpecFile}" ;
file GTAPUPD  = "{updateFile}" ;

! output file
file OUTFILE  = "{landChangeHarFile}" ;

auxiliary files = {landChangeBasename};
'''

def runLUC(args):
    '''
    Run the LUC tablo program to generate landchange.har in the current directory; return exit status
    '''
    if getParamAsBoolean('SkipGempackCommands'):
        _logger.warning('Skipping GEMPACK commands as per config file')
        return SUCCESS

    exePath = getParam('LandChangeExePath')
    cmfFile = getParam('LandChangeCmfPath')
    harFile = getParam('LandChangeHarFile')
    updFile = getParam('UpdateFile')
    lucBase = getParam('LandChangeBasename')
    gtapDir = getParam('GtapDir')
    landChangeDir = getParam('LandChangeDir')
    cropSpecFile  = getParam('CropSpecHarFile')

    # if os.path.lexists(harFile) and os.path.getmtime(harFile) >= os.path.getmtime(updFile):
    #     _logger.debug("File %s is up-to-date" % harFile)
    #     return SUCCESS

    mkdirs(os.path.dirname(harFile))

    # generate the cmf file based on arguments / config settings.
    cmfText = lucCmfTemplate.format(gtapDir=gtapDir,
                                    lucDir=landChangeDir,
                                    updateFile=updFile,
                                    landChangeBasename=lucBase,
                                    landChangeHarFile=harFile,
                                    cropSpecFile=cropSpecFile)
    with open(cmfFile, 'w') as f:
        f.write(cmfText)

    logFile = getParam('LandChangeLogPath')
    args = [exePath, '-cmf', cmfFile, '-los', logFile]
    cmd = " ".join(args)
    _logger.info("Running %r", cmd)

    try:
        status = call(args, shell=False)
        _logger.debug("runLUC: %s exited with status %d", exePath, status)

    except Exception, e:
        status = FAILURE
        _logger.error("runLUC: exception while running %r: %s", cmd, e)

    return status


# For use in interactive mode
def _setDisplayOpts(decimalPlaces=0):
    fmtStr = '{:,.%df}' % decimalPlaces
    pd.options.display.float_format = fmtStr.format
    pd.set_option('display.width', 1000)


def runAEZEF(args):
    '''
    Run the AEZEF model
    '''
    _setDisplayOpts(decimalPlaces=0)    # for interactive (debugging) purposes

    experiments = map(str.strip, map(str, args.experiments.split(','))) # converts unicode to str first
    outputDir = args.outputDir

    randomize = getParamAsBoolean('RandomizeTransitions')
    results = {exp : [] for exp in experiments} if randomize else {}

    trials = getParamAsInt('TrialCount') if randomize else 1

    for exp in experiments:
        setSection(exp)
        setParam('Experiment', exp)

        lucStatus = runLUC(args)
        if lucStatus != SUCCESS:
            return lucStatus

        # N.B. runLUC catches and reports exceptions
        _logger.debug("runAEZEF: running AEZEF for %s", exp)
        for i in xrange(trials):
            m = Model(args)

            if randomize:
                results[exp].append(m.ilucIntensity)
            else:
                results[exp] = m
                csvDir = os.path.join(outputDir, exp)
                m.writePlotData(csvDir)

                if args.plot:
                    plotSummaries(csvDir)

    if randomize:
        df = pd.DataFrame(data=results)
        df.to_csv(getParam('RandomTransitionResultsFile'))
        df.hist(layout=(1, len(experiments)))
    else:
        saveToExcel(results)

    return SUCCESS

#
# firstGroup = [
#     (ANN, CP),
#     (SUG, CP),
#     (SWG, CP),
#     (MSC, CP),
#     (POP, CP),
#     (PLM, CP),
# ]
#
# randomGroup = [
#     (CP, ANN), # randomize this block
#     (CP, SUG),
#     (CP, SWG),
#     (CP, MSC),
#     (CP, POP),
#     (CP, PLM),
# ]
#
# restOfGroups = [
#     (SWG, MSC),
#     (MSC, SWG),
#     (SWG, POP),
#     (MSC, POP),
#     (POP, SWG),
#     (POP, MSC),
#
#     # Next, transitions between annual, perennials, and grasses.
#     # We assume no transitions between palm & sugar and cellulosic crops.
#     (ANN, SUG),
#     (ANN, PLM),
#     (ANN, SWG),
#     (ANN, MSC),
#     (ANN, POP),
#
#     (SUG, ANN),
#     (PLM, ANN),
#     (SWG, ANN),
#     (MSC, ANN),
#     (POP, ANN),
#
#     # Allow changes between perennials
#     (SUG, PLM),
#     (PLM, SUG),
#
#     # Transitions between crops and pasture
#     (SWG, PAS),
#     (MSC, PAS),
#     (POP, PAS),
#     (PAS, SWG),
#     (PAS, MSC),
#     (PAS, POP),
#
#     (ANN, PAS),
#     (SUG, PAS),
#     (PLM, PAS),
#     (PAS, ANN),
#     (PAS, SUG),
#     (PAS, PLM),
#
#     # Transitions between pasture and forestry
#     (FOR, PAS),
#     (PAS, FOR),
#
#     # Transitions between forestry and tree crops
#     (FOR, POP),
#     (FOR, PLM),
#     (POP, FOR),
#     (PLM, FOR),
#
#     # Transitions between forestry and the rest
#     (FOR, ANN),
#     (FOR, SUG),
#     (FOR, SWG),
#     (FOR, MSC),
#     (ANN, FOR),
#     (SUG, FOR),
#     (SWG, FOR),
#     (MSC, FOR),
# ]
