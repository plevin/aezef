'''
@author: Richard Plevin

Copyright (c) 2017 Richard Plevin. See the file COPYRIGHT.txt for details.
'''
from __future__ import print_function
from logging import getLogger

from .config import getParamAsFloat
from .error import AezefException
from .utils import (
    gtapDataFrame, getFileParams, CO2PerC, setUnits,
    FOR, PAS, CP, ANN, PLM, OPP, SUG, SWG, MSC, POP
)

_logger = getLogger(__name__)

ANNUAL     = [ANN]
PERENNIAL  = [SUG, PLM]
CELLULOSIC = [SWG, MSC, POP]
CROPLAND   = ANNUAL + PERENNIAL + CELLULOSIC
LAND_USES  = CROPLAND + [FOR, PAS, CP]


def _readSequestrationRates():
    from pkg_resources import resource_string
    from StringIO import StringIO
    import pandas as pd

    data = resource_string('aezef', 'etc/annualSeqRates100cm.csv')
    df = pd.read_csv(StringIO(data))

    df['AEZ'] = 'AEZ' + df.AEZ.map(str)    # Change, e.g., 7 => "AEZ7"

    setUnits(df, 'Mg C/ha/y')
    return df


def _cellulosicSoilCarbonEFs():
    df = _readSequestrationRates()
    efDict = {}

    years = getParamAsFloat('CellulosicSoilEquilibriumYears')
    socFactor = getParamAsFloat('CroplandPastureSoilFactor')

    for idx, row in df.iterrows():
        key = (row.old, row.new)

        # Alter CP-to-{SWG,MSC,POP} SOC changes by multiplying by the factor.
        # This is used for sensitivity analysis only. (Default value is 1.0).
        if socFactor != 1.0 and row.old == CP and row.new in CELLULOSIC:
            row.value *= socFactor

        if key not in efDict:
            efDict[key] = gtapDataFrame()

        ef = efDict[key]

        # Reverse the sign convention so sequestration is negative and emission is positive
        ef.ix[row.AEZ, :] = -1 * row.value * years
        setUnits(ef, 'Mg C/ha')

    return efDict

# Emissions for these transitions are calculated as -1
# times those of the reverse sequence.
Negations = [
    (ANN, CP),
    (ANN, FOR),
    (SUG, FOR),
    (PLM, FOR),
    (SUG, PAS),
    (PLM, PAS),
    (SUG, CP),
    (PLM, CP),
    (SWG, ANN),
    (SWG, FOR),
    (SWG, PAS),
    (SWG, CP),
    (MSC, ANN),
    (MSC, FOR),
    (MSC, PAS),
    (MSC, SWG),
    (MSC, CP),
    (POP, ANN),
    (POP, FOR),
    (POP, PAS),
    (POP, SWG),
    (POP, CP),
]


class Transition(object):
    _instances = {}     # keyed by name

    biomassChanges = {}   # Losses and gains keyed by original ("old") land-use

    cellSoilChanges = _cellulosicSoilCarbonEFs()

    model = None

    def __init__(self, oldLU, newLU):
        self.old = oldLU
        self.new = newLU
        self.name = self.createName(oldLU, newLU)
        self.key = (oldLU, newLU)

        # True if emissions for this transition are simply -1 * the reverse
        self.negate = self.key in Negations

        self.area = gtapDataFrame()
        self.ef   = None
        self.soilChangeDF = None    # These are computed once and cached
        self.emissions = None

    def __str__(self):
        return self.name

    @classmethod
    def decache(cls):
        cls._instances = {}
        cls.biomassChanges = {}
        cls.model = None

    @classmethod
    def createName(cls, oldLU, newLU):
        return oldLU + '_TO_' + newLU

    @classmethod
    def initialize(cls, model):
        """
        Perform one-time calculations required by Transition instances
        """
        cls.model = model

    @classmethod
    def get(cls, oldLU, newLU):
        name = cls.createName(oldLU, newLU)
        try:
            obj = cls._instances[name]
        except KeyError:
            obj = Transition(oldLU, newLU)
            cls._instances[name] = obj

        return obj

    @classmethod
    def computeEmissions(cls):
        for obj in cls._instances.itervalues():
            obj.emissions = obj.ef * obj.area   #  / 1000000.0   # convert to Tg (MMT)
            setUnits(obj.emissions, 'Mg CO2e')  # 'Tg CO2e')

    @classmethod
    def biomassChangeToCellulosicGHG(cls, oldLU):
        """
        N.B. Called only for transitions to cellulosic crops
        """
        try:
            return cls.biomassChanges[oldLU]
        except KeyError:
            pass

        if oldLU in CROPLAND:
            # Handled later via yield-based calc of crop mass change
            df = gtapDataFrame()  # zeros

        elif oldLU == FOR:  # (FOR, {SWG, MSC, POP})
            df = cls.model.forest.weightedConversionToPerennials_GHG   # biomass gains/losses only

        elif oldLU == PAS:
            df = cls.model.pasture.biomassLoss_GHG

        elif oldLU == CP:   # TBD: Assumed to have the same biomass as active pasture
            df = cls.model.pasture.biomassLoss_GHG

        cls.biomassChanges[oldLU] = df
        setUnits(df, 'Mg CO2e/ha')
        return df

    def soilCarbonChangeToCellulosic(self):
        if self.soilChangeDF is None:
            # Uses Qin et al. (2016) modeling results
            self.soilChangeDF = self.cellSoilChanges[self.key]

        return self.soilChangeDF

    def emissionFactors(self):
        if self.ef is not None:
            return self.ef # return cached result

        old = self.old
        new = self.new
        seq = (old, new)
        forest  = self.model.forest
        pasture = self.model.pasture

        if self.negate:
            # negate emissions for opposite transition
            t = Transition.get(new, old)
            df = -1 * t.emissionFactors()

        elif seq == (FOR, ANN):
            df = forest.weightedConversionToAnnuals_GHG
        elif seq == (FOR, SUG) or seq == (FOR, PLM):
            df = forest.weightedConversionToPerennials_GHG
        elif seq == (FOR, OPP):
            df = forest.weightedConversionToPerennials_GHG + forest.totalPeatLoss_GHG
        elif seq == (FOR, PAS):
            df = forest.conversionToPasture_GHG
        elif seq == (PAS, ANN):
            df = pasture.conversionToAnnuals_GHG
        elif seq == (PAS, SUG) or seq == (PAS, PLM):
            df = pasture.conversionToPerennials_GHG
        elif seq == (PAS, OPP):
            df = pasture.conversionToPerennials_GHG + pasture.totalPeatLoss_GHG
        elif seq == (PAS, FOR):
            df = pasture.weightedConversionToForest_GHG
        elif seq == (ANN, PAS):
            df = pasture.annualsReversion_GHG

        elif old in CELLULOSIC:
            # Cellulosics appear in GTAP baseline only because they
            # must be non-zero to permit change; there were none
            # in the real data. We treat this as transition from CP.
            proxy = CP   # TBD: Could be ANN.

            if new == proxy:
                df = gtapDataFrame()    # zeros => no change
            else:
                t = Transition.get(proxy, new)
                df = t.emissionFactors()

        elif new in CELLULOSIC:
            # biomass loss is based only on old land-use
            biomassLoss_GHG = self.biomassChangeToCellulosicGHG(old)

            socChange = self.soilCarbonChangeToCellulosic()
            N2O_ghg   = self.model.forest.carbonLossN2O_GHG(socChange)
            df = (socChange * CO2PerC) + biomassLoss_GHG + N2O_ghg

        # Note: original (CARB) model did not model transitions between CP and pasture.
        elif {old, new} in [{ANN, SUG}, {ANN, PLM}, {SUG, PLM}, {CP, PAS}]:
            # These are expected to be small, so we assume zero
            df = gtapDataFrame()    # zeros

        elif old == CP and new in (ANN, SUG, PLM):
            # As in original AEZ-EF, emissions for transitions from CP are
            # half those from pasture.
            t = Transition.get(PAS, new)
            p = getFileParams()
            df = t.emissionFactors() * p.croplandPastureEmissionRatio

        else:
            raise AezefException('Unrecognized transition sequence (%s, %s)' % seq)

        setUnits(df, 'Mg CO2e/ha')
        df.fillna(0.0, inplace=True)
        self.ef = df  # cache the result
        return df


