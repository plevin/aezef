#!/usr/bin/env python
'''
.. Created on: 4/10/17
   Derived from pygcam's chart.py module

.. Copyright (c) 2015-2017 Richard Plevin
   See the https://opensource.org/licenses/MIT for license details.
'''
from __future__ import print_function
import matplotlib as mpl

major, minor, rev = map(int, mpl.__version__.split('.'))
if major >= 2:
    import platform
    if platform.system() == 'Darwin':
        mpl.use('TkAgg')

import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

from glob import glob
import os
import pandas as pd
import seaborn as sns

from .utils import mkdirs, setUnits

# from logging import getLogger
# _logger = getLogger(__name__)

#%matplotlib inline


def plotGtapData(df, box=False, rotation=90, zeroLine=False, title="",
                 xlabel='', ylabel='', ncol=5, ygrid=True, yticks=False,
                 ymin=None, ymax=None, barWidth=0.5, legendY=None, outFile=None,
                 sideLabel=False, labelColor=None, yFormat=None):
    '''
    Plot a stacked bar plot using data in df, given the index column, the
    column holding the values to pivot to columns, and the column holding
    the values. The argument 'ncol' specifies the number of columns with
    which to render the legend.
    '''
    df = df.transpose()
    sns.set_palette(sns.color_palette("hls", len(df.columns)))

    fig, ax = plt.subplots(1, 1, figsize=(8, 4))
    df.plot(kind='bar', stacked=True, ax=ax, grid=False, width=barWidth, rot=rotation)

    if box == False:
        sns.despine(left=True)

    if yticks:
        plt.tick_params(axis='y', direction='out', length=5, width=.75,
                        colors='k', left='on', right='off')

    if zeroLine:
        ax.axhline(0, color='k', linewidth=0.75, linestyle='-')

    if ygrid:
        ax.yaxis.grid(color='lightgrey', linestyle='solid')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    legendY = -0.4 if legendY is None else legendY
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, legendY), ncol=ncol)

    if title:
        ax.set_title(title, y=1.05)

    if ymin is not None or ymax is not None:
        ax.set_autoscale_on(False)
        ax.set_ylim(ymin, ymax)

    if yFormat:
        func = (lambda x, p: format(int(x), ',')) if yFormat == ',' else (lambda x, p: yFormat % x)
        formatter = FuncFormatter(func)
        ax.get_yaxis().set_major_formatter(formatter)

    if sideLabel:
        labelColor = labelColor or 'lightgrey'
        # add the filename down the right side of the plot
        fig.text(1, 0.5, sideLabel, color=labelColor, weight='ultralight', fontsize=7,
                 va='center', ha='right', rotation=270)

    if outFile:
        fig.savefig(outFile, bbox_inches='tight', transparent=False)

    plt.close(fig)


def plotSummaries(csvDir):
    sns.set_context("paper", font_scale=1.5)
    sns.set_style("white")

    outputDir = os.path.join(csvDir, 'figs')

    mkdirs(outputDir)

    # Can't sum EFs over AEZs...
    # ef='Mg CO$_2$e ha$^{-1}$',
    unitMap = dict(area='ha', ghg='Tg CO$_2$e')

    luMap = {
        'ann' : 'Annual crops',
        'for' : 'Forestry',
        'pas' : 'Pasture',
        'cp'  : 'Cropland-pasture',
        'sug' : 'Sugarcane',
        'plm' : 'Oil palm',
        'msc' : 'Miscanthus',
        'swg' : 'Switchgrass',
        'pop' : 'Poplar'
    }

    # ef='Emission factors',
    categoryMap = dict(area='Area', ghg='Emissions')

    for category, units in unitMap.items():
        categoryName = categoryMap[category]

        csvDict = {}
        regionMin = regionMax = 0

        # read them all first so we can set consistent bounds
        for csvFile in glob(csvDir + '/%s-*.csv' % category):
            df = pd.read_csv(csvFile, index_col=0)

            # Convert emissions from Mg to Tg for plotting
            if category == 'ghg':
                df /= 1.0E6
                setUnits(df, 'Tg CO2e')

            csvDict[csvFile] = df


            regionSums = df.sum()
            regionMaxs = df.max()
            regionMins = df.min()

            regionMax = max(regionMaxs.max(), regionSums.max(), regionMax)
            regionMin = min(regionMins.min(), regionSums.min(), regionMin)

        for csvFile, df in csvDict.iteritems():
            basename = os.path.basename(csvFile)
            base, ext = os.path.splitext(basename)
            outFile = os.path.join(outputDir, base + '.png')
            print("Generating %s" % outFile)

            oldLU = base.split('-')[1]
            luName = luMap[oldLU]
            if category == 'area':
                title = 'Area converted from %s' % luName
            else:
                title = "%s for conversion of %s" % (categoryName, luName)

            plotGtapData(df, outFile=outFile, title=title, sideLabel=outFile,
                         ymin=regionMin, ymax=regionMax, ylabel=units,
                         yFormat=',', zeroLine=True)


if __name__ == '__main__':
    csvDir = '/Users/rjp/tmp/aezef/'
    plotSummaries(csvDir)
