'''
.. aezef's exception classes

.. Copyright (c) 2017 Richard Plevin
   See the https://opensource.org/licenses/MIT for license details.
'''
class AezefException(Exception):
    """
    Base class for pygcam Exceptions.
    """
    pass

class FileMissingError(AezefException):
    """
    Indicate that a required file was not found or not readable.
    """

    def __init__(self, filename, reason):
        self.filename = filename
        self.reason   = reason

    def __str__(self):
        return "Can't read %s: %s" % (self.filename, self.reason)

class FileFormatError(AezefException):
    """
    Indicate a syntax error in a user-managed file.
    """
    pass

class FileExistsError(AezefException):
    """
    Raised when trying to write a file that already exists (if not allowed)
    """
    def __init__(self, filename):
        self.filename = filename

    def __str__(self):
        return "Refusing to overwrite file: %s" % self.filename

class ConfigFileError(FileFormatError):
    """
    Raised when an error is found in the configuration file ``~/.pygcam.cfg``.
    """
    pass

class CommandlineError(Exception):
    """
    Command-line arguments were missing or incorrectly specified.
    """
    pass

class ProgramExecutionError(AezefException):
    """
    Raised when attempt to execute a program (e.g., gempack utility) fails.
    """
    def __init__(self, command, exitCode=None, reason=None):
        self.command = command
        self.exitCode = exitCode
        self.reason = reason

    def __str__(self):
        s = "Command '%s' failed" % self.command

        if self.exitCode is not None:
            s += " with exit code %s" % self.exitCode

        if self.reason is not None:
            s += ": %s" % self.reason

        s += '.'
        return s
