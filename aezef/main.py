'''
.. Copyright (c) 2015-2017 Richard Plevin
   See the https://opensource.org/licenses/MIT for license details.
'''
import argparse
from logging import getLogger, StreamHandler

from .config import getConfig, getParam, getParamAsBoolean, setParam
from .error import AezefException
from .model import runAEZEF

_logger = getLogger(__name__)

def parseArgs():

    parser = argparse.ArgumentParser(description='''Run the AEZEF model''')

    dumpData = getParamAsBoolean('LandChangeDumpData')
    parser.add_argument('-d', '--dumpData', action='store_true',
                        help='''Dump intermediate data to a file for debugging. Default
                        is the value of config variable LandChangeDumpData, currently
                        "%s"''' % dumpData)

    experiments = getParam('Experiments')
    parser.add_argument('-e', '--experiments', default=experiments, metavar='name',
                        help='''The name of the GTAP experiments to evaluate. Default is
                         the value of config variable Experiments, currently
                         "%s".''' % experiments)

    # gtapDir = getParam('GtapDir')
    # parser.add_argument('-g', '--gtapDir', default=gtapDir, metavar='pathname',
    #                     help='''The location of the GTAP directory with results
    #                     to process. Defaults to the value of config variable GtapDir,
    #                     currently set to "%s".''' % gtapDir)
    #
    # landChangeDir = getParam('LandChangeDir')
    # parser.add_argument('-l', '--landChangeDir', metavar='pathname', default=landChangeDir,
    #                     help='''The directory holding the "LUC" TABLO program and associated
    #                     files. Defaults to the value of config variable LandChangeDir,
    #                     currently set to "%s".''' % landChangeDir)

    logLevel = getParam('LogLevel')
    parser.add_argument('-L', '--logLevel', default=logLevel,
                        choices=['debug', 'info', 'warn', 'error', 'critical', 'fatal'],
                        help='''The logging level. Defaults to the value of config
                        variable LogLevel, currently set to "%s".''' % logLevel)

    outputDir = getParam('OutputDir')
    parser.add_argument('-o', '--outputDir', default=outputDir,
                        help='''Where to save aggregated CSV files and resulting plots''')

    parser.add_argument('-p', '--plot', action='store_true',
                        help='''Plot summary figures''')

    args = parser.parse_args()
    return args


def main():
    try:
        getConfig()
        args = parseArgs()

        # Setup logger for top-level of the package
        log = getLogger('aezef')
        log.addHandler(StreamHandler())
        log.setLevel(args.logLevel.upper())

        return runAEZEF(args)

    except AezefException as e:
        print("Error: %s" % e)
        if getParamAsBoolean('ShowStackTrace'):
            import traceback
            traceback.print_exc()


if __name__ == '__main__':
    import sys
    sys.exit(main())
