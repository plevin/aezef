'''
@author: Richard Plevin

Copyright (c) 2012-2017. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
from logging import getLogger
import pandas as pd

from .config import getParamAsInt
from .utils import (
    N2OPerN, CO2PerC, CO_OXIDATION_FACTOR, VOC_OXIDATION_FACTOR,
    gtapDataFrame
)

_logger = getLogger(__name__)

IPCC_CroplandManagementFactor = 1.0
IPCC_CroplandInputFactor      = 1.0
IPCC_GrasslandLandUseFactor   = 1.0

# IPCC data refers to young stands as those < 20 years old
YOUNG_STAND_MAX_YEARS  = 20


def pairwise(fn, df1, df2):
    '''
    Apply the function `fn` element-wise to the two DataFrames, producing a third.
    :param fn: a function of two arguments that returns a single value.
    :param df1: the DataFrame that provides the first argument to the function `fn`
    :param df2: the DataFrame that provides the first argument to the function `fn`
    :return:
    '''
    assert df1.shape == df2.shape, 'Pairwise operation requires 2 DataFrames with the same shape'

    df3 = pd.DataFrame(index=df1.index, columns=df1.columns)
    for col, df1Series in df1.iteritems():
        df2Series = df2[col]
        df3Series = df3[col]
        for row, df1Value in df1Series.iteritems():
            df2Value = df2Series[row]
            df3Series[row] = fn(df1Value, df2Value)

    return df3


def broadcastByLatitude(fromLatitude, mask=None):
    '''
    Copy values from a Series with "latitude" based rows into one with AEZ-based rows.
    '''
    toAEZ = gtapDataFrame()

    toAEZ.loc['AEZ1' :'AEZ6']  = float(fromLatitude.loc['Tropical'])
    toAEZ.loc['AEZ7' :'AEZ12'] = float(fromLatitude.loc['Temperate'])
    toAEZ.loc['AEZ13':'AEZ18'] = float(fromLatitude.loc['Boreal'])

    return toAEZ if mask is None else toAEZ * mask

#
# The ForestToCrop/PastureToCrop classes merely provide aliases for underlying
# data specific to forest and pasture so the "generic" calculation methods in
# LandConversion can handle both.
#
class LandConversion(object):
    def __init__(self, model):
        self.params           = model.params            # simplify access to exogenous parameters
        self.landChange       = model.landChange
        #self.transitions      = model.transitions
        self.landUseFactor    = 1.0
        self.managementFactor = 1.0
        self.inputLevelFactor = 1.0
        self.combustionFactor = None

        self.peatAreas = gtapDataFrame(dtype=bool, data=False)
        self.peatAreas.Mala_Indo['AEZ4':'AEZ6'] = True

        self.ipccSoilChange_C     = None
        self.subsoilLoss_C        = None
        self.soil_C               = None
        self.subsoil_C            = None
        self.soilLossFraction     = None
        self.subsoilLossFraction  = None
        self.soilLossToAnnuals_C  = None
        self.litter_C             = None
        self.agb_C                = None
        self.bgb_C                = None
        self.burningEF            = None
        self.carbonFraction       = None
        self.combustionFactor     = None
        self.deadwoodByRegion_C   = None
        self.deadwoodByLatitude_C = None
        self.foregoneGrowthRate   = None
        self.deadwood_C           = None
        self.understory_C         = None
        self.hwpFraction          = None

        self.analyticHorizonYears = getParamAsInt('AnalyticHorizonYears')
        self.foregoneSequestYears = getParamAsInt('ForegoneSequestYears')


    def carbonLossN2O_GHG(self, carbonLoss):
        '''
        Compute the N2O emissions (as CO2e) associated with a given carbon loss DataFrame.
        '''
        p = self.params

        N2O_EF = p.N2O_N_EF * N2OPerN * p.GWP_N2O
        soilN2O = carbonLoss / p.carbonNitrogenRatio * N2O_EF
        soilN2O[(soilN2O < 0)] = 0  # zero out negative emissions
        return soilN2O


    def getBiomassRegrowth(self):
        assert False, "subclass responsibility"


    def computeSoilCarbonChange(self):
        """
        IPCC approach to soil change and associated algebra:

          changeInSOC = SOC_before - SOC_after

          SOC_before = SOC_reference * [landUseFactor_before * managementFactor_before * inputFactor_after]
          SOC_after  = SOC_reference * [landUseFactor_after  * managementFactor_after  * inputFactor_after]
              (call the bracketed terms productOfChangeFactors_before and productOfChangeFactors_after)

          SOC_reference = SOC_before / productOfChangeFactors_before = SOC_after / productOfChangeFactors_after
          SOC_after = SOC_before * productOfChangeFactors_after / productOfChangeFactors_before

          changeInSOC = SOC_before - SOC_before * productOfChangeFactors_after / productOfChangeFactors_before
                      = SOC_before * (1 - (productOfChangeFactors_after / productOfChangeFactors_before))

        Note: referenceSOC is the SOC level in "natural" land, neither degraded nor improved,
        under natural vegetation in the area in question. We factor this out.
        """
        p = self.params

        # Below, productOfStockChangeFactors is the product for either forest or pasture, depending
        # which subclass is active, and soil_C refers to the subclass' topsoil C stock, i.e., the
        # state before conversion to cropland.
        self.productOfCroplandStockChangeFactors = p.croplandLandUseFactor * IPCC_CroplandManagementFactor * IPCC_CroplandInputFactor
        self.productOfStockChangeFactors = self.landUseFactor * self.managementFactor * self.inputLevelFactor
        self.ipccSoilChange_C = self.soil_C * (1 - (self.productOfCroplandStockChangeFactors / self.productOfStockChangeFactors))

        # Latitude-based topsoil loss calculation. Use the product of soil_C and soilLossFraction
        # where the loss fraction is defined, else use IPCC value computed above.
        self.topsoilLoss_C = self.soil_C * self.soilLossFraction
        mask = self.topsoilLoss_C.isnull()
        self.topsoilLoss_C[mask] = self.ipccSoilChange_C[mask]

        # Subsoil loss is given by Poeplau et al. (2011) as a fraction of the total SOC loss (non-zero
        # only for some pasture to cropland). Other transitions showed little change in subsoil.
        # The algebra:
        #
        #   subsoilLoss = subsoilLossFraction * (subsoilLoss + topsoilLoss)
        #   subsoilLoss = subsoilLossFraction * subsoilLoss + subsoilLossFraction * topsoilLoss
        #   (1 - subsoilLossFraction) * subsoilLoss = topsoilLoss * subsoilLossFraction
        #   subsoilLoss = topsoilLoss * subsoilLossFraction / (1 - subsoilLossFraction)
        #
        self.subsoilLoss_C = self.topsoilLoss_C * self.subsoilLossFraction / (1 - self.subsoilLossFraction)
        self.subsoilLoss_C.fillna(0.0, inplace=True)

        self.soilLossToAnnuals_C = self.topsoilLoss_C + self.subsoilLoss_C

        # Cropland reversion to forest or pasture. Same equation as above for conversion to cropland,
        # but the before and after states are flipped, so SOC_before is for cropland, and the stock
        # change factor ratio is the reciprocal.
        #
        # Original approach used this location's average cropland carbon as the starting point
        #
        convertedCroplandSoil_C = p.croplandSoil_C

        # Alt approach uses the computed (post-conversion) cropland SOC rather than regional average
        # ensure that the conversion/reversion dynamics start are consistent. We assume that subsoil
        # does not regain the lost carbon in the 30 years considered here.
        #
        # convertedCroplandSoil_C = self.soil_C - self.topsoilLoss_C

        self.soilReversion_C = convertedCroplandSoil_C * (1 - (self.productOfStockChangeFactors / self.productOfCroplandStockChangeFactors))

    def computeCarbonChange(self):
        p = self.params

        # biomass changes
        self.totalBiomass_C = self.agb_C + self.bgb_C + self.understory_C + self.deadwood_C + self.litter_C
        self.HWP_C          = self.agb_C * self.hwpFraction
        self.fuel_C         = (self.agb_C + self.deadwood_C + self.litter_C - self.HWP_C) * p.activeRegion  # no understory
        self.combusted_C    = self.fuel_C * p.fireClearingFraction * self.combustionFactor
        self.uncombusted_C  = self.totalBiomass_C - self.combusted_C - self.HWP_C

        EF = self.burningEF
        burningGHG = ((EF['CO2'] + EF['CO'] * CO_OXIDATION_FACTOR + EF['NMHC'] * VOC_OXIDATION_FACTOR) * p.GWP_CO2 +
                       EF['CH4'] * p.GWP_CH4 + EF['N2O'] * p.GWP_N2O)
        self.combustion_GHG = self.combusted_C / self.carbonFraction / 1000 * broadcastByLatitude(burningGHG, mask=p.activeRegion)
        self.decay_GHG = self.uncombusted_C * CO2PerC

        # Call method to allow pasture and forest to behave differently
        self.biomassRegrowth_C = self.getBiomassRegrowth()                      # values are negative, indicating sequestration
        self.biomassLoss_GHG   = self.combustion_GHG + self.decay_GHG

        self.foregone_C   = self.foregoneGrowthRate * self.foregoneSequestYears
        self.foregone_GHG = self.foregone_C * CO2PerC

        # Cropland biomass is handled separately based on post-simulation yields take from GTAP
        # and converted to total live biomass using factors from West, Brandt, et al. (2010).
        self.biomassReversion_C = self.biomassRegrowth_C - (self.understory_C + self.deadwood_C) # litter is included in reversion

        self.computeSoilCarbonChange()

        # Total C gain for cropland reversion to forest/pasture.
        self.annualsReversion_C    = self.biomassReversion_C + self.soilReversion_C
        self.perennialsReversion_C = self.biomassReversion_C

        self.annualPeatLoss_C = p.MalaIndoPeatEF / CO2PerC
        self.totalPeatLoss_C  = gtapDataFrame()
        self.totalPeatLoss_C[self.peatAreas] = self.annualPeatLoss_C * self.analyticHorizonYears

        # These are used when assigning emission factors to transitions
        self.peatLossN2O_GHG   = self.carbonLossN2O_GHG(self.totalPeatLoss_C)
        self.totalPeatLoss_GHG = self.totalPeatLoss_C * CO2PerC + self.peatLossN2O_GHG


    def computeConversionToCropland(self):
        '''
        Generalized calculation of GHG changes from conversion from pasture or forest
        to cropland. Elements that differ between these are handled in the subclasses
        ForestToCrop and PastureToCrop.
        '''
        p = self.params
        self.computeCarbonChange()

        # N2O emissions associated with carbon losses
        self.soilN2O_GHG  = self.carbonLossN2O_GHG(self.soilLossToAnnuals_C)
        self.soilLoss_GHG = self.soilLossToAnnuals_C * CO2PerC + self.soilN2O_GHG

        self.totalSoilAndBiomass_GHG = self.biomassLoss_GHG + self.soilLoss_GHG

        # We no longer subtract out cropland biomass here since it's handled at the end, based on GTAP's revised yields
        self.conversionToAnnuals_GHG = self.totalSoilAndBiomass_GHG + self.foregone_GHG

        # Assumes conversion of forest/pasture to perennials doesn't affect soil
        self.conversionToPerennials_GHG = self.biomassLoss_GHG + self.foregone_GHG

        self.annualsReversion_GHG    = self.annualsReversion_C    * CO2PerC
        self.perennialsReversion_GHG = self.perennialsReversion_C * CO2PerC


class ForestToCrop(LandConversion):
    def __init__(self, model):
        super(ForestToCrop, self).__init__(model)
        #
        # Set "generic" names to forest-specific values
        #
        p = self.params

        self.soil_C              = p.forestSoil_C
        self.subsoil_C           = p.forestSubsoil_C
        self.soilLossFraction    = p.forestSoilLossFraction
        self.subsoilLossFraction = p.forestSubsoilLossFraction
        self.litter_C            = p.forestLitter_C
        self.agb_C               = p.forestAgb_C
        self.bgb_C               = p.forestBgb_C
        self.burningEF           = p.forestBurningEF
        self.carbonFraction      = p.woodyCarbonFraction
        self.combustionFactor    = broadcastByLatitude(p.forestCombustionFactor, mask=p.activeRegion)

        # Work from local copies of these parameters; PastureToCrop zeros them out
        self.deadwoodByRegion_C   = p.deadwoodByRegion_C
        self.deadwoodByLatitude_C = p.deadwoodByLatitude_C
        self.foregoneGrowthRate   = p.foregoneGrowthRate
        self.deadwood_C           = p.deadwood_C
        self.understory_C         = p.understory_C
        self.hwpFraction          = p.hwpFraction

    def getBiomassRegrowth(self):
        p = self.params
        mostOfTheBiomass = self.totalBiomass_C - p.excludedLitterFraction * self.litter_C
        return -1 * pairwise(min, mostOfTheBiomass, p.regrowth_C)

    def computeConversionToCropland(self):
        super(ForestToCrop, self).computeConversionToCropland()

        # Weighted average of deforestation and avoided afforestation
        deforestedFraction = self.params.deforestedFraction
        afforestedFraction = (1 - deforestedFraction)
        self.weightedConversionToAnnuals_GHG    = deforestedFraction * self.conversionToAnnuals_GHG    + afforestedFraction * -1 * self.annualsReversion_GHG
        self.weightedConversionToPerennials_GHG = deforestedFraction * self.conversionToPerennials_GHG + afforestedFraction * -1 * self.perennialsReversion_GHG

    def computeConversionToPasture(self, pasture):
        '''
        Compute forest-to-pasture conversion. These are the same as for Forestry-to-cropland
        minus the soil C change, and substituting post-conversion pasture biomass for crop biomass.
        '''
        # This is not the final value, but one of the values used by pasture's P-to-F calculation
        self.conversionToPasture_GHG = self.biomassLoss_GHG + self.foregone_GHG - (pasture.totalBiomass_C * CO2PerC)


class PastureToCrop(LandConversion):
    def __init__(self, model):
        super(PastureToCrop, self).__init__(model)

        #
        # Set "generic" names to pasture-specific values
        #
        p = self.params

        self.soil_C              = p.pastureSoil_C
        self.subsoil_C           = p.pastureSubsoil_C
        self.soilLossFraction    = p.pastureSoilLossFraction
        self.subsoilLossFraction = p.pastureSubsoilLossFraction
        self.burningEF           = p.pastureBurningEF
        self.carbonFraction      = p.grassCarbonFraction
        self.litter_C            = p.pastureLitter_C * p.activeRegion

        # Zero out some calcs for pasture. No need to define
        # as a dataframe, a scalar value of 0 does the job.
        self.deadwoodByRegion_C     = 0
        self.deadwoodByLatitude_C   = 0
        self.foregoneGrowthRate     = 0
        self.deadwood_C             = 0
        self.understory_C           = 0
        self.hwpFraction            = 0

        self.agb_C = p.pastureAgb * self.carbonFraction
        self.bgb_C = p.pastureBgb * self.carbonFraction
        self.biomass_C = self.agb_C + self.bgb_C
        self.combustionFactor = broadcastByLatitude(p.pastureCombustionFactor, mask=p.activeRegion)


    def getBiomassRegrowth(self):
        return -1 * self.totalBiomass_C

    def computeConversionToForest(self, forest):
        self.forestReversion_GHG    = (forest.biomassRegrowth_C + self.totalBiomass_C) * CO2PerC
        self.conversionToForest_GHG = -forest.conversionToPasture_GHG
        deforestedFraction = self.params.deforestedFraction
        afforestedFraction = (1 - deforestedFraction)
        self.weightedConversionToForest_GHG = deforestedFraction * self.conversionToForest_GHG + \
                                              afforestedFraction * self.forestReversion_GHG
