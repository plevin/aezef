'''
@author: Richard Plevin

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
from __future__ import print_function
from logging import getLogger
import numpy as np
import os
from platform import uname
import re
import six
from subprocess  import call
from tempfile import mkstemp

from .config import getParam, getParamAsBoolean
from .error import FileExistsError, FileFormatError, AezefException, ProgramExecutionError

_logger = getLogger(__name__)


class GempackError(AezefException):
    def __init__(self, programName, exitStatus):
        self.programName = programName
        self.exitStatus = exitStatus

    def __str__(self):
        return "Gempack program '%s' exited with status %d" % (self.programName, self.exitStatus)


def makeHAR(harPath, textFile=None, targetDir=".", saveDefault=False):
    '''
    Invokes the GEMPACK "modhar" program to convert the text-format
    parameter file containing new trial data into the binary HAR format.
    If a textFile isn't named, it's assumed to be the same as the harFile
    but with the extension changed to ".txt".
    '''
    if uname()[0] == 'Darwin':      # We don't have modhar for the Mac
        _logger.info("Faking makeHAR(%s, %s, targetDir='%s', saveDefault=%s", harPath, textFile, targetDir, saveDefault)
        return

    if not textFile:
        (base, ext) = os.path.splitext(harPath)
        textFile = base + '.txt'

    stiFile = 'modhar.sti'
    stiData = """bat  !  This runs modhar in "batch" mode

n
%s
at
%s
A
ex
0
""" % (harPath, textFile)

    modharPath = getParam('ModharPath')

    cwd = os.getcwd()  # save current working trialDir to return from excursions

    _logger.debug("cwd=%r" % cwd)
    _logger.debug("Running '%s' in dir '%s'" % (modharPath, targetDir))

    os.chdir(targetDir)

    try:
        # Create the temporary sti file in the current directory
        with open(stiFile, 'w') as f:
            f.write(stiData)

        status = call([modharPath, '-sti', stiFile, '-los', 'modhar.log'])
        if not saveDefault:
            os.remove(stiFile)

    finally:            # restore prior working directory
        os.chdir(cwd)
        # N.B. temp files not removed (by design) if modhar fails

    if status:
        raise GempackError('modhar', status)

    if not saveDefault:
        os.remove(textFile)
        os.remove(os.path.join(targetDir, 'modhar.log'))


def seehar(harFile, txtFile=None, format='sss', tmpDir=".", saveTmpFiles=False):
    '''
    Run the GEMPACK 'seehar' program to convert a HAR file to the given
    text format.
    '''
    if getParamAsBoolean('SkipGempackCommands'):
        _logger.debug('Skipping Gempack command (seehar)')
        return

    if not txtFile:
        fd, txtFile = mkstemp(suffix='.csv', dir=tmpDir, text=True)
        os.close(fd)       # we need the unique name, not the open file

    fd, stiFile = mkstemp(suffix='.sti', dir=tmpDir, text=True)
    os.close(fd)

    harFile = os.path.abspath(harFile)

    stiData = """bat  !  This runs seehar in "batch" mode
%s


%s
%s
r
""" % (format, harFile, txtFile)

    seeharPath = getParam('SeeharPath')

    cwd = os.getcwd()  # save current working trialDir to return from excursions

    _logger.debug("Running '%s' in dir '%s'" % (seeharPath, tmpDir))

    os.chdir(tmpDir)

    try:
        # Create the temporary sti file in the current directory
        with open(stiFile, 'w') as f:
            f.write(stiData)

        args = [seeharPath, '-sti', stiFile, '-los', 'seehar.log']
        _logger.debug("Invoking %s", args)
        status = call(args, shell=False)
        if not saveTmpFiles:
            os.remove(stiFile)
            os.remove('seehar.log')

    finally:            # restore prior working directory
        os.chdir(cwd)
        # N.B. temp files not removed (by design) if seehar fails

    if status:
        raise GempackError('seehar', status)

    return txtFile


def readHarFile(filename, hdrlist=None, fmt=None, overwrite=False, deleteTxt=True):
    '''
    Read the data for the given headers (identified by shortname) from the name HAR file.
    If the hdrlist is None or an empty list, all headers in the file are read and returned.
    Returns a dict of numpy arrays. Currently handles only 1, 2, or 3 dimensions. If
    the filename is any of the usual HAR extensions {'.har', '.upd', '.prm'} we run
    the GEMPACK program har2txt to produce a text version of the file. If the extension
    is '.txt', we assume it's already been converted. User must provide the format arg
    fmt (which must be either 'har' or 'txt') if the extension is something other than
    {'.har', '.upd', '.prm', '.txt'}.
    '''
    _logger.debug("readHarFile(%s,%s)" % (filename, hdrlist))

    # User specifies format without leading period
    txtFmt  = 'txt'
    harFmt  = 'har'
    allFmts = (txtFmt, harFmt)

    harExts = ('.har', '.upd', '.prm')
    txtExts = ('.txt',)
    allExts = harExts + txtExts

    root, ext = os.path.splitext(filename)

    if fmt is None:
        if not ext:
            raise FileFormatError("readHarFile: no file extension on header file '%s' and no format specified" % filename)

        if ext not in allExts:
            raise FileFormatError("readHarFile: unrecognized header file extension '%s'" % ext)

        fmt = txtFmt if ext in txtExts else harFmt
    else:
        if fmt not in allFmts:
            raise FileFormatError("readHarFile: unrecognized header file format '%s" % fmt)

    # if the file is in HAR format, convert it to text
    if fmt == harFmt:
        # har2txt isn't part of the source distro so it's not in GempackBin, thus the config var.
        txtfile = root + '.txt'

        # Don't overwrite the text file unless 'overwrite' is True
        if not overwrite and os.path.isfile(txtfile):
            raise FileExistsError(txtfile)

        if os.path.lexists(txtfile) and os.path.getmtime(txtfile) >= os.path.getmtime(filename):
            _logger.info("File %s is up-to-date" % txtfile)
        else:
            har2txtPath = getParam('Har2txtPath')

            if not os.path.lexists(har2txtPath):
                raise AezefException('''har2txt not found at "%s".
            Please fix config variable "Har2txtPath" in your aezef.cfg file.''' % har2txtPath)

            _logger.info("Running program: %s %s %s" % (har2txtPath, filename, txtfile))

            status = 1
            args = [har2txtPath, filename, txtfile]

            try:
                status = call(args)

            except Exception as e:
                cmd = ' '.join(args)
                raise ProgramExecutionError(cmd, reason=e)

            if status != 0:
                try:
                    os.unlink(txtfile)
                except:
                    pass
                raise GempackError(har2txtPath, status)

    else:
        # it's already a text file
        txtfile = filename

    hdrs = readHar2TxtFile(txtfile, hdrlist)

    if txtfile and deleteTxt:
        os.remove(txtfile)

    return hdrs


def readHar2TxtFile(txtfile, hdrlist):
    _logger.debug("Reading '%s'" % os.path.abspath(txtfile))

    # Allow caller to pass either a list or a single header name.
    if hdrlist and isinstance(hdrlist, six.string_types):
        hdrlist = [hdrlist]

    hdrs = {}

    with open(txtfile, 'r') as f:
        while True:
            line = f.readline()
            if len(line) == 0:
                break
            desc = line.rstrip()

            m = re.match(r'^((\d+\s+)+)(Real|Integer|Strings)\s+((Spreadsheet|Length)\s*(\d+)?)\s+header\s+"(\S{1,4}\s{0,3})"\s+LongName\s+"(.*)"\s*;',
                         desc, flags=re.IGNORECASE)
            if m:
                sname = m.group(7).rstrip()
                if hdrlist and not sname in hdrlist:    # if no header names are provide, read them all
                    # _logger.debug("Skipping header '%s'", sname)
                    continue

                #_logger.debug("Reading header '%s'", sname)

                dims   = map(int, m.group(1).split())
                dimcnt = len(dims)
                if dimcnt > 3:
                    raise FileFormatError("readHar2TxtFile: Parameter has %d dimensions; we can read only 3" % dimcnt)

                gptype = m.group(3)
                if gptype == 'Strings':
                    raise FileFormatError("readHar2TxtFile: can't read string vectors")
            else:
                continue

            rows   = dims[0]
            cols   = dims[1] if dimcnt > 1 else 1
            blocks = dims[2] if dimcnt > 2 else 1

            # Read the header description into a 3D array
            data = np.zeros((blocks, rows, cols))

            for block in xrange(blocks):
                # Read a matrix of values
                for row in xrange(rows):
                    line   = f.readline().rstrip()
                    values = line.split(',') if cols > 1 else [line]
                    if len(values) != cols:
                        raise FileFormatError("readHar2TxtFile: Expected %d columns of data, but got %d" % (cols, len(values)))

                    data[block][row] = map(float, values)

                if blocks > 1:
                    # When there are multiple blocks, the end with a blank line
                    line = f.readline().rstrip()
                    if line != '':
                        raise FileFormatError("readHar2TxtFile: expected blank line at the end of a block, found '%s'" % line)

            hdrs[sname] = data

    if (hdrlist and len(hdrs) != len(hdrlist)):
        raise FileFormatError("readHar2TxtFile: tried to read headers %s, got %s" % (hdrlist, hdrs.keys()))

    return hdrs


def _dumpHeader(name, data):
    '''
    Prints the contents of a 3-dim numpy matrix as space-separated rows of tab-separated columns
    '''
    dims   = data.shape
    dimcnt = len(dims)

    blocks = dims[0] if dimcnt == 3 else 1
    rows   = dims[1] if dimcnt == 3 else dims[0]

    print("Header: %s" % name, dims)
    for block in xrange(blocks):
        for row in xrange(rows):
            print("\t".join( map(str, data[block][row])))
        print("")
