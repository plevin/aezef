'''
@author: Richard Plevin

Copyright (c) 2014-2017. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
from __future__ import print_function
from logging import getLogger
import pandas as pd

from .config import getParam
from .error import FileFormatError

_logger = getLogger(__name__)

# Atomic weights from http://physics.nist.gov/cgi-bin/Compositions/stand_alone.pl
CARBON_MASS   = 12.0 # 12.0107
OXYGEN_MASS   = 16.0 # 15.9994
NITROGEN_MASS = 14.0 # 14.0067

CO2_MASS      = CARBON_MASS + 2 * OXYGEN_MASS
CO_MASS       = CARBON_MASS + OXYGEN_MASS
CO2PerC       = CO2_MASS / CARBON_MASS
N2O_MASS      = 2 * NITROGEN_MASS + OXYGEN_MASS
N2OPerN       = N2O_MASS / (2 * NITROGEN_MASS)

VOC_C_FRACTION       = 0.85
VOC_OXIDATION_FACTOR = VOC_C_FRACTION * CO2PerC
CO_OXIDATION_FACTOR  = CO2_MASS / CO_MASS

# Header names for landchange.har
FORESTRY    = 'CFOR'
CROPLAND    = 'CCRP'
PASTURE     = 'CLVS'
ALLCROPS    = 'ALLC'
CROPYIELD   = 'YLDA'
TOTCROPC    = 'TLBC'

# Names of specially-handled crops
CROPPAST    = 'Pasturecrop'
SUGARCROP   = 'Sugar_Crop'
OILPALM     = 'palmf'
SWITCHGRASS = 'Switchgrass'
MISCANTHUS  = 'Miscanthus'
POPLAR      = 'Poplar'

SpecialCrops = [CROPPAST, SUGARCROP, OILPALM, SWITCHGRASS, MISCANTHUS, POPLAR]

LAND_HEADERS = [ALLCROPS, FORESTRY, CROPLAND, PASTURE, TOTCROPC]

FOR = 'FOR'
PAS = 'PAS'
CP  = 'CP'
ANN = 'ANN'
PLM = 'PLM' # oil palm not on peat
SUG = 'SUG'
SWG = 'SWG'
MSC = 'MSC'
POP = 'POP'
OPP = 'OPP' # oil palm on peat

CELLULOSIC = [SWG, MSC, POP]
LAND_USES  = [FOR, PAS, ANN, PLM, SUG] + CELLULOSIC

GasolineMJperGal       = 122.48     # conversion by google

# Deprecated?
# FAME_MJ_PER_GAL        = 126.0
# ETOH_MJ_PER_GAL        = 81.33
# BIOGASOLINE_MJ_PER_GAL = 122.37     # GREET1_2015: 115,983 Btu/gal LHV for Renewable Gasoline
# FT_JET_FUEL_MJ_PER_GAL = 126.37     # GREET1_2015: 119,777 Btu/gal LHV for SPK (FT Jet Fuel/HRJ)
# FT_DIESEL_MJ_PER_GAL   = 130.48     # GREET1_2015: 123,670 Btu/gal LHV for FT Diesel
#
# FuelDensity = {
#     'fame'          : FAME_MJ_PER_GAL,
#     'bd'            : FAME_MJ_PER_GAL,
#     'biodiesel'     : FAME_MJ_PER_GAL,
#     'ethanol'       : ETOH_MJ_PER_GAL,
#     'etoh'          : ETOH_MJ_PER_GAL,
#     'biogasoline'   : BIOGASOLINE_MJ_PER_GAL,
#     'bio-gasoline'  : BIOGASOLINE_MJ_PER_GAL,
#     'ft-diesel'     : FT_DIESEL_MJ_PER_GAL,
#     'ft-jet-fuel'   : FT_JET_FUEL_MJ_PER_GAL,
# }
#
# def fuelDensityMjPerGal(fuelname):
#     '''
#     Return the fuel energy density of the named fuel.
#
#     :param fuelname: the name of a fuel (currently must be one of {fame,bd,ethanol,etoh,biogasoline,bio-gasoline}.
#     :return: energy density (MJ/gal)
#     '''
#     return FuelDensity[fuelname.lower()]

AEZ = map(lambda n: "AEZ%d" % n, range(1, 19))

# Canonical region names
REG = ["USA", "EU27", "Brazil","Canada", "Japan", "ChiHkg", "India", "C_C_Amer",
       "S_O_Amer", "E_Asia", "Mala_Indo", "R_SE_Asia", "R_S_Asia", "Russia",
       "Oth_CEE_CIS", "Oth_Europe", "MEas_NAfr", "S_S_Afr", "Oceania"]

# Key is upper-cased GTAP-BIO-ADV region name
_CanonicalRegions = {reg.upper() : reg for reg in REG}
_CanonicalRegions['CAN'] = 'Canada' # add this exception

def canonicalizeRegion(reg):
    return _CanonicalRegions[reg.upper()]


def gtapDataFrame(dtype=float, data=0.0):
    '''
    Create a DataFrame indexed by AEZ, with regions for columns

    :param dtype: data type
    :param data: Initial value for new DataFrame.
    :return: DataFrame
    '''
    import pandas as pd     # lazy import

    return pd.DataFrame(columns=REG, index=AEZ, dtype=dtype, data=data)


def dumpObj(obj, indent=0):
    prefix = " " * indent

    for k in sorted(obj.__dict__.iterkeys()):
        v = obj.__dict__[k]
        valueFmt = '%.3f' if type(v) == float else '%r'
        fmt = "%25s = " + valueFmt
        print(prefix, end='')
        print(fmt % (k, v))


class Namespace(object):
    '''
    Syntactic sugar to allow access as ns.forestAgb_C rather than as
    dict['forestAgb_C'].
    '''
    def __init__(self, aDict):
        self.dict = aDict

    # Called when attr not found in self.__dict__
    # Allows access to parameters as if attributes
    def __getattr__(self, item):
        return self.dict[item]


def _readParams(fp, filename):
    from io import BytesIO

    lineNum = 0
    paramDict = {}

    for line in fp:
        lineNum += 1
        items = line.rstrip().split('\t')
        tag   = items[0].lower()

        # identify scalars as just "name  value", i.e., whenever there are just 2 items
        if len(items) == 2:
            name, value = items
            value = float(value)
            paramDict[name] = value
            continue

        # Header format is either "skip rowCount" or "variableName rowCount colCount"
        if tag == 'skip':
            if len(items) not in (2, 3):    # handle legacy case of unnecessary cols arg
                raise FileFormatError("%s, line %d: SKIP header must indicate the number of lines to ignore" % (filename, lineNum))

            count = int(items[1])
            for _ in xrange(count):
                next(fp)
            continue

        # otherwise tag is variable name followed by rows and cols, then a matrix with column headers.
        if len(items) != 3:
            raise FileFormatError("%s, line %d: Variable header must indicate the rows and cols in the subsequent matrix" % (filename, lineNum))

        name = items[0]
        rows = int(items[1])
        cols = int(items[2])
        lines = [next(fp) for _ in xrange(rows+1)]  # add 1 for column header row
        lines = map(str.rstrip, lines)
        buffer = '\n'.join(lines)
        df = pd.read_table(BytesIO(buffer), index_col=0, na_values=['NA'])
        lineNum += rows+1
        if len(df.columns) != cols:
            raise FileFormatError("%s: line %d: for variable %s, expected %d cols, found %d" % (filename, lineNum, name, cols, len(df.columns)))

        func = canonicalizeRegion if rows == 18 and cols == 19 else str.upper
        df.columns = map(func, df.columns)
        paramDict[name] = df

    return paramDict


_Params = None

def getFileParams():
    '''
    Read the AEZ-EF parameter file. By default, reads into DataFrames.

    :param filename: The full path to the parameter file
    :return: dictionary of DataFrames & scalars, keyed by parameter name
    '''
    from pkg_resources import resource_stream

    global _Params

    if not _Params:
        filename = getParam('ParameterFile')

        if filename:
            _logger.debug("Reading AEZEF data: %s", filename)
            with open(filename) as fp:
                paramDict = _readParams(fp, filename)
        else:
            filename = 'etc/aezef.param'
            _logger.debug("Reading internal AEZEF data from %s", filename)
            fp = resource_stream('aezef', filename)
            paramDict = _readParams(fp, filename)

        _Params = Namespace(paramDict)

    return _Params


UNITS_IVAR = '_aezefUnits'

def setUnits(obj, units):
    '''
    Set an instance variable indicating units (usually on a DataFrame)
    '''
    setattr(obj, UNITS_IVAR, units)

def getUnits(obj):
    return getattr(obj, UNITS_IVAR, '')

def mkdirs(newdir, mode=0o770):
    """
    Try to create the full path `newdir` and ignore the error if it already exists.

    :param newdir: the directory to create (along with any needed parent directories)
    :return: nothing
    """
    import os
    from errno import EEXIST

    try:
        os.makedirs(newdir, mode)
    except OSError as e:
        if e.errno != EEXIST:
            raise
